package commonsconfigurations.mavenproject4;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;



public class BgColor {

    private static final String Properties = "configuration.properties";

    public BgColor() {
    }

    
    public static void main(String[] args) throws ConfigurationException {
        BgColor application = new BgColor();
        application.createProperties();
        application.setBackground("#c53479");
        String background = application.getBackground();
        System.out.println("background-color:" + background);
    }

    
    public void createProperties() throws ConfigurationException {
        
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.save(Properties);
    }

    
    public void setBackground(String backgroundColor) throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.load(Properties);
        if (backgroundColor == null) {
            config.setProperty("colors.background", "#000000");
        } else {
            config.setProperty("colors.background", backgroundColor);
        }
        config.save(Properties);
    }

    
    public String getBackground() throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.load(Properties);
        String background = (String) config.getProperty("colors.background");
        return background;
    }
}