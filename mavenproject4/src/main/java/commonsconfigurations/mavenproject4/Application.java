package commonsconfigurations.mavenproject4;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;



public class Application {

    private static final String PROPERTIES_FILE_NAME = "configuration.properties";

    /**
     * <p>Default no-arg constructor.</p>
     */
    public Application() {
    }

    /**
     * <p>Main application entry point.</p>
     * @param args
     * @throws ConfigurationException
     */
    public static void main(String[] args) throws ConfigurationException {
        Application application = new Application();
        application.createProperties();
        application.setBackground("#c53478");
        String background = application.getBackground();
        System.out.println("background-color:" + background);
    }

    /**
     * <p>Creates a property file on the local system disk.</p>
     * @throws ConfigurationException if an <code>Exception</code> is encountered.
     */
    public void createProperties() throws ConfigurationException {
        // Create a new property and save it.
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.save(PROPERTIES_FILE_NAME);
    }

    /**
     * <p>Sets a property called <code>colors.background</code> to the value provided, or assigns a color
     * value{@literal #000000} if none is provided .</p>
     * @param backgroundColor
     * @throws ConfigurationException if an <code>Exception</code> is encountered. 
     */
    public void setBackground(String backgroundColor) throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.load(PROPERTIES_FILE_NAME);
        if (backgroundColor == null) {
            config.setProperty("colors.background", "#000000");
        } else {
            config.setProperty("colors.background", backgroundColor);
        }
        config.save(PROPERTIES_FILE_NAME);
    }

    /**
     * <p>Retrieves the background property from the properties file on
     * the system disk.</p>
     * @return the background color assigned in the properties file.
     * @throws ConfigurationException if an <code>Exception</code> is encountered.
     */
    public String getBackground() throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.load(PROPERTIES_FILE_NAME);
        String background = (String) config.getProperty("colors.background");
        return background;
    }
}